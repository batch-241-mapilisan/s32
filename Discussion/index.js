const http = require('http');
const port = 3000;
const server = http.createServer((req, res) => {
    if (req.url == '/items' && req.method == 'GET') {
        res.writeHead(200, {
            'Content-Type': 'text/plain'
        })
        res.end('Data retrieved from the database')
    } else if (req.url == '/items' && req.method == 'POST') {
        res.writeHead(200, {
            'Content-Type': 'text/plain'
        })
        res.end('Data was created and sent to the database')
    }
})

server.listen(port)

console.log(`Server is up and running on port ${port}`);